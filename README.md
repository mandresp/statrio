# README #

MAFIA application allows users to manage and control the MAFIA

### What is this repository for? ###

* Quick summary: You can see a resume in mafia.pdf file placed in docs folder on parent module
* Version 0.0.1-SNAPSHOT

### How do I get set up? ###

* To build this app write in CMD on parent module: mvn clean intall
* Configuration: No extra configuration needed 
* Database configuration: MAFIA es powered by H2, you can find and modify the scripts in boot module
* Deployment instructions: To deploy the app write in CMD mvn spring-boot:run
* How to run tests : MAFIA is powered with SWAGGER. You can run it!! http://localhost:8080/swagger-ui.html
* All Methods are documented by SWAGGER

### Who do I talk to? ###

* mandrpaz@gmail.com 