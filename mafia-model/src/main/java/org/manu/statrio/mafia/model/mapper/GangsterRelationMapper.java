package org.manu.statrio.mafia.model.mapper;

import java.util.List;

import org.manu.statrio.mafia.model.api.dto.GangsterRelationDto;
import org.manu.statrio.mafia.model.jpa.entity.GangsterRelation;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface GangsterRelationMapper {

	GangsterRelation toEntity(GangsterRelationDto source);

	GangsterRelationDto toDto(GangsterRelation target);

	List<GangsterRelation> toEntityList(List<GangsterRelationDto> source);

	List<GangsterRelationDto> toDtoList(List<GangsterRelation> target);

}
