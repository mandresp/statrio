package org.manu.statrio.mafia.model.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.manu.statrio.mafia.model.api.dto.GangsterDto;
import org.manu.statrio.mafia.model.api.service.GangsterService;
import org.manu.statrio.mafia.model.jpa.repository.GangsterRepository;
import org.manu.statrio.mafia.model.mapper.GangsterMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GangsterServiceImpl implements GangsterService {

	@Autowired
	private GangsterRepository gangsterRepository;

	@Autowired(required = false)
	private GangsterMapper gangsterMapper;

	@Override
	public List<GangsterDto> findAll() {

		return gangsterMapper.toDtoList(
				StreamSupport.stream(gangsterRepository.findAll().spliterator(), false).collect(Collectors.toList()));
	}

	public GangsterDto findOne(Integer id) {
		return gangsterMapper.toDto(gangsterRepository.findById(id).orElse(null));
	}

	@Override
	public List<GangsterDto> findByReplacementId(Integer id) {
		return gangsterMapper.toDtoList(gangsterRepository.findByIdReplacement(id));
	}

	@Override
	public List<GangsterDto> findByRank(Short id) {
		return gangsterMapper.toDtoList(gangsterRepository.findByRank(id));
	}

	@Override
	public GangsterDto saveGangster(GangsterDto dto) {

		return gangsterMapper.toDto(gangsterRepository.save(gangsterMapper.toEntity(dto)));

	}

	@Override
	public List<GangsterDto> findMany(List<Integer> ids) {
		return gangsterMapper.toDtoList(StreamSupport.stream(gangsterRepository.findAllById(ids).spliterator(), false)
				.collect(Collectors.toList()));
	}

	@Override
	public void deleteGangster(GangsterDto gangster) {
		gangsterRepository.delete(gangsterMapper.toEntity(gangster));
	}

}