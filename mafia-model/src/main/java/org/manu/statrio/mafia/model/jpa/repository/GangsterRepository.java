package org.manu.statrio.mafia.model.jpa.repository;

import java.util.List;

import org.manu.statrio.mafia.model.jpa.entity.Gangster;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface GangsterRepository extends CrudRepository<Gangster, Integer> {

	/**
	 * @param id
	 * @return jailed bosses
	 */
	@Query
	List<Gangster> findByIdReplacement(Integer id);

	/**
	 * @param id
	 * @return gangster
	 */
	@Query
	List<Gangster> findByRank(Short id);

}