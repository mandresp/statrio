package org.manu.statrio.mafia.model.service;

import java.util.List;

import org.manu.statrio.mafia.model.api.dto.GangsterRelationDto;
import org.manu.statrio.mafia.model.api.service.GangsterRelationService;
import org.manu.statrio.mafia.model.jpa.repository.GangsterRelationRepository;
import org.manu.statrio.mafia.model.mapper.GangsterRelationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GangsterRelationServiceImpl implements GangsterRelationService {

	@Autowired
	private GangsterRelationRepository gangsterRelationRepository;

	@Autowired(required = false)
	private GangsterRelationMapper gangsterRelationMapper;

	@Override
	public List<GangsterRelationDto> findByBoss(Integer id) {
		return gangsterRelationMapper.toDtoList(gangsterRelationRepository.findByIdGangsterBoss(id));
	}

	@Override
	public GangsterRelationDto saveRelation(GangsterRelationDto rel) {
		return gangsterRelationMapper.toDto(gangsterRelationRepository.save(gangsterRelationMapper.toEntity(rel)));
	}

	@Override
	public GangsterRelationDto findBySubordinateId(Integer id) {
		return gangsterRelationMapper.toDto(gangsterRelationRepository.findByIdGangsterSubordinate(id));
	}

	@Override
	public void saveRelations(List<GangsterRelationDto> subs) {
		gangsterRelationRepository.saveAll(gangsterRelationMapper.toEntityList(subs));
	}

	@Override
	public void deleteRelation(GangsterRelationDto bossRel) {
		gangsterRelationRepository.delete(gangsterRelationMapper.toEntity(bossRel));
	}

}