package org.manu.statrio.mafia.model.jpa.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(includeFieldNames = true)
@Entity
public class GangsterRelation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false, unique = true, name = "creation_time")
	private Date creationTime;
	@Column(nullable = false, unique = true, name = "id_gangster_boss")
	private Integer idGangsterBoss;
	@Column(nullable = false, unique = true, name = "id_gangster_subordinate")
	private Integer idGangsterSubordinate;

}
