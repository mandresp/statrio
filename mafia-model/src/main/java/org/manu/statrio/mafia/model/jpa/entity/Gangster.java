package org.manu.statrio.mafia.model.jpa.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(includeFieldNames = true)
@Entity
public class Gangster {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(nullable = false, unique = true, name = "first_name")
	private String firstName;
	@Column(nullable = false, unique = true, name = "last_name")
	private String lastName;
	@Column(nullable = false, unique = true, name = "rank")
	private Short rank;
	@Column(nullable = false, unique = true, name = "imprisoned")
	private boolean imprisoned;
	@Column(nullable = false, unique = true, name = "join_date")
	private Date joinDate;
	@Column(nullable = true, unique = true, name = "id_gangster_replacement")
	private Integer idReplacement;

}
