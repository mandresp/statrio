package org.manu.statrio.mafia.model.jpa.repository;

import java.util.List;

import org.manu.statrio.mafia.model.jpa.entity.GangsterRelation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface GangsterRelationRepository extends CrudRepository<GangsterRelation, Integer> {

	@Query
	List<GangsterRelation> findByIdGangsterBoss(Integer id);

	@Query
	GangsterRelation findByIdGangsterSubordinate(Integer id);

}