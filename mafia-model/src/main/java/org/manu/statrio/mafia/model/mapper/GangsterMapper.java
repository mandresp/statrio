package org.manu.statrio.mafia.model.mapper;

import java.util.List;

import org.manu.statrio.mafia.model.api.dto.GangsterDto;
import org.manu.statrio.mafia.model.jpa.entity.Gangster;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface GangsterMapper {

	Gangster toEntity(GangsterDto source);

	GangsterDto toDto(Gangster target);

	List<Gangster> toEntityList(List<GangsterDto> source);

	List<GangsterDto> toDtoList(List<Gangster> target);

}
