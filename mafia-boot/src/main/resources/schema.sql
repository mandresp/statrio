DROP TABLE IF EXISTS gangster;
DROP TABLE IF EXISTS gangster_rank;
DROP TABLE IF EXISTS ganster_relation;


CREATE TABLE gangster_rank (
  id SMALLINT AUTO_INCREMENT  PRIMARY KEY, 
  label VARCHAR(250) NOT NULL 
);
 
  
CREATE TABLE gangster (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250),
  rank SMALLINT NOT NULL,
  imprisoned BOOLEAN NOT NULL DEFAULT FALSE,
  id_gangster_replacement INT DEFAULT NULL,
  join_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP()
  
);
 
  
CREATE TABLE gangster_relation(
  id INT AUTO_INCREMENT PRIMARY KEY,
  id_gangster_boss INT NOT NULL,
  id_gangster_subordinate INT NOT NULL,
  creation_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP()
);

ALTER TABLE gangster_relation 
ADD CONSTRAINT ganster_relation_unique
UNIQUE ( id_gangster_boss, id_gangster_subordinate );
 
