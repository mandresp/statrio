package org.manu.statrio.mafia.model.api.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(includeFieldNames = true)
public class GangsterDto {

	private Integer id;

	private String firstName;
	private String lastName;
	private Short rank;
	private boolean imprisoned;
	private Date joinDate;

	private Integer idReplacement;

}
