package org.manu.statrio.mafia.model.api.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(includeFieldNames = true)
public class GangsterRelationDto {

	private Integer id;

	private Date creationTime;
	private Integer idGangsterBoss;
	private Integer idGangsterSubordinate;

}
