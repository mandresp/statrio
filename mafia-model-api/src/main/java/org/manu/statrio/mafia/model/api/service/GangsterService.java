package org.manu.statrio.mafia.model.api.service;

import java.util.List;

import org.manu.statrio.mafia.model.api.dto.GangsterDto;

public interface GangsterService {

	/**
	 * @return All Gangster s
	 */
	List<GangsterDto> findAll();

	/**
	 * @param id
	 * @return Gangster data
	 */
	GangsterDto findOne(Integer id);

	/**
	 * @param id
	 * @return jailed bosses
	 */
	List<GangsterDto> findByReplacementId(Integer id);

	/**
	 * @param id
	 * @return soldier
	 */
	List<GangsterDto> findByRank(Short id);

	/**
	 * @param dto
	 */
	GangsterDto saveGangster(GangsterDto dto);

	/**
	 * @param collect
	 * @return gangstersById
	 */
	List<GangsterDto> findMany(List<Integer> ids);

	/**
	 * @param gangster
	 */
	void deleteGangster(GangsterDto gangster);

}
