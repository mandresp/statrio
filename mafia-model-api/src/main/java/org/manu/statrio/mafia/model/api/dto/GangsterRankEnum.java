package org.manu.statrio.mafia.model.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Getter
@NoArgsConstructor

public enum GangsterRankEnum {

	SOLDIER((short) 1, "Soldier"), CAPO((short) 2, "Capo"), CAPO_DI_CAPI((short) 3, "Capo di Capi"),
	PADRINO((short) 4, "Padrino");

	Short id;
	String label;

	/**
	 * @param id
	 * @return Rank by id
	 */
	public static GangsterRankEnum getGangsterRankEnumById(Short id) {
		for (GangsterRankEnum e : values()) {
			if (e.id.equals(id))
				return e;
		}
		return null;
	}
}
