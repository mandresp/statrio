package org.manu.statrio.mafia.model.api.service;

import java.util.List;

import org.manu.statrio.mafia.model.api.dto.GangsterRelationDto;

public interface GangsterRelationService {

	/**
	 * @param id
	 * @return
	 */
	List<GangsterRelationDto> findByBoss(Integer id);

	/**
	 * @param rel
	 */
	GangsterRelationDto saveRelation(GangsterRelationDto rel);

	/**
	 * @param id
	 * @return rel
	 */
	GangsterRelationDto findBySubordinateId(Integer id);

	/**
	 * @param subs
	 */
	void saveRelations(List<GangsterRelationDto> subs);

	/**
	 * @param bossRel
	 */
	void deleteRelation(GangsterRelationDto bossRel);

}
