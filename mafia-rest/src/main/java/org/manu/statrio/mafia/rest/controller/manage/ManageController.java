package org.manu.statrio.mafia.rest.controller.manage;

import org.manu.statrio.mafia.core.api.service.MafiaService;
import org.manu.statrio.mafia.rest.api.manage.Gangster4AddDto;
import org.manu.statrio.mafia.rest.api.manage.IManageController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api()
public class ManageController implements IManageController {

	@Autowired
	private MafiaService mafiaService;

	@Override
	@ApiOperation(httpMethod = "POST", value = "Add a Gangster")
	public ResponseEntity<Object> add(Gangster4AddDto g) {
		mafiaService.addGangster(g.getFirstName(), g.getLastName(), g.getBoss());
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	@ApiOperation(httpMethod = "PUT", value = "Release a Gangster from prison")
	public ResponseEntity<Object> release(@ApiParam("Gangster ID") Integer id) {
		mafiaService.releaseGangster(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	@ApiOperation(httpMethod = "PUT", value = "Send a Gangster to prison")
	public ResponseEntity<Object> jail(@ApiParam("Gangster ID") Integer id) {
		mafiaService.jailGangster(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Override
	@ApiOperation(httpMethod = "DELETE", value = "Kill a Gangster")
	public ResponseEntity<Object> kill(@ApiParam("Gangster ID") Integer id) {
		mafiaService.killGangster(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
