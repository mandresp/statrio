package org.manu.statrio.mafia.rest.search.mapper;

import java.util.List;

import org.manu.statrio.mafia.core.api.dto.GangsterBossesDto;
import org.manu.statrio.mafia.core.api.dto.GangsterSubordinatesDto;
import org.manu.statrio.mafia.model.api.dto.GangsterDto;
import org.manu.statrio.mafia.model.api.dto.GangsterRankEnum;
import org.manu.statrio.mafia.rest.api.search.Gangster4ViewDto;
import org.manu.statrio.mafia.rest.api.search.GangsterBosses4ViewDto;
import org.manu.statrio.mafia.rest.api.search.GangsterSubordinatesHierarchy4ViewDto;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface Gangster4ViewMapper {

	Gangster4ViewDto toView(GangsterDto source);

	GangsterDto toDto(Gangster4ViewDto target);

	List<Gangster4ViewDto> toViewList(List<GangsterDto> source);

	List<GangsterDto> toDtoList(List<Gangster4ViewDto> target);

	GangsterSubordinatesHierarchy4ViewDto toViewSubordinatesHierarchy(GangsterSubordinatesDto findFullHierarchy);

	List<GangsterBosses4ViewDto> toViewBigBossesList(List<GangsterBossesDto> findBigBosses);

	@AfterMapping
	default void changeColor(@MappingTarget Gangster4ViewDto gangsterDto) {
		gangsterDto.setRankName(GangsterRankEnum.getGangsterRankEnumById(gangsterDto.getRank()).getLabel());
	}

	@AfterMapping
	default void changeColor(@MappingTarget GangsterSubordinatesHierarchy4ViewDto gangsterDto) {
		gangsterDto.setRankName(GangsterRankEnum.getGangsterRankEnumById(gangsterDto.getRank()).getLabel());
	}

	@AfterMapping
	default void changeColor(@MappingTarget GangsterBosses4ViewDto gangsterDto) {
		gangsterDto.setRankName(GangsterRankEnum.getGangsterRankEnumById(gangsterDto.getRank()).getLabel());
	}

}
