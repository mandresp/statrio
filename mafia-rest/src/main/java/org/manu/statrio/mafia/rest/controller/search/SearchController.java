package org.manu.statrio.mafia.rest.controller.search;

import java.util.List;

import org.manu.statrio.mafia.core.api.service.MafiaService;
import org.manu.statrio.mafia.model.api.service.GangsterService;
import org.manu.statrio.mafia.rest.api.search.Gangster4ViewDto;
import org.manu.statrio.mafia.rest.api.search.GangsterBosses4ViewDto;
import org.manu.statrio.mafia.rest.api.search.GangsterSubordinatesHierarchy4ViewDto;
import org.manu.statrio.mafia.rest.api.search.ISearchController;
import org.manu.statrio.mafia.rest.search.mapper.Gangster4ViewMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@Api()
public class SearchController implements ISearchController {

	@Autowired
	private GangsterService gangsterService;

	@Autowired
	private MafiaService mafiaService;

	@Autowired
	private Gangster4ViewMapper gangster4ViewMapper;

	@Override
	@ApiOperation(httpMethod = "GET", value = "Find all Gangsters")
	public ResponseEntity<List<Gangster4ViewDto>> findAll() {
		List<Gangster4ViewDto> lst = gangster4ViewMapper.toViewList(gangsterService.findAll());
		return new ResponseEntity<>(lst, HttpStatus.OK);

	}

	@Override
	@ApiOperation(httpMethod = "GET", value = "Find Gangsters with more than 50 subordinates")
	public ResponseEntity<List<GangsterBosses4ViewDto>> findBigBosses() {

		List<GangsterBosses4ViewDto> lst = gangster4ViewMapper.toViewBigBossesList(mafiaService.findBigBosses());

		return new ResponseEntity<>(lst, HttpStatus.OK);
	}

	@Override
	@ApiOperation(httpMethod = "GET", value = "Find a Gangster by ID as JSON tree")
	public ResponseEntity<Gangster4ViewDto> findOne(@ApiParam("Gangster ID") Integer id) {
		return new ResponseEntity<>(gangster4ViewMapper.toView(gangsterService.findOne(id)), HttpStatus.OK);

	}

	@Override
	@ApiOperation(httpMethod = "GET", value = "Find Hierarchy of a Gangster by ID as JSON tree")
	public ResponseEntity<Gangster4ViewDto> findHierarchy(@ApiParam("Gangster ID") Integer id) {
		return new ResponseEntity<>(gangster4ViewMapper.toViewSubordinatesHierarchy(mafiaService.findHierarchy(id)),
				HttpStatus.OK);
	}

	@Override
	@ApiOperation(httpMethod = "GET", value = "Find Hierarchy of the PADRINO")
	public ResponseEntity<GangsterSubordinatesHierarchy4ViewDto> findFullHierarchy() {
		return new ResponseEntity<>(gangster4ViewMapper.toViewSubordinatesHierarchy(mafiaService.findFullHierarchy()),
				HttpStatus.OK);
	}
}
