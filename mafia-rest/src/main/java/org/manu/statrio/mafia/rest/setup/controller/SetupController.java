package org.manu.statrio.mafia.rest.setup.controller;

import org.manu.statrio.mafia.rest.api.setup.ISetupController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SetupController implements ISetupController {

	@Override
	public ResponseEntity<Boolean> isAppUp() {
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
