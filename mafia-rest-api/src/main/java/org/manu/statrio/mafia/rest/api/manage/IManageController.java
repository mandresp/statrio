package org.manu.statrio.mafia.rest.api.manage;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/manage")
public interface IManageController {

	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<Object> add(@RequestBody Gangster4AddDto g);

	@PutMapping(value = "/jail/{id}")
	ResponseEntity<Object> jail(@PathVariable Integer id);

	@DeleteMapping(value = "/kill/{id}")
	ResponseEntity<Object> kill(@PathVariable Integer id);

	@PutMapping(value = "/release/{id}")
	ResponseEntity<Object> release(@PathVariable Integer id);

}
