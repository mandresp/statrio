package org.manu.statrio.mafia.rest.api.search;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(includeFieldNames = true)
public class GangsterSubordinatesHierarchy4ViewDto extends Gangster4ViewDto {

	List<GangsterSubordinatesHierarchy4ViewDto> subordinates;

}
