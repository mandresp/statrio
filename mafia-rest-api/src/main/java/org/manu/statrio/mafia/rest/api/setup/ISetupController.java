package org.manu.statrio.mafia.rest.api.setup;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/setup")
public interface ISetupController {

	/**
	 * @return 200
	 */

    @GetMapping("/")
	ResponseEntity<Boolean> isAppUp();

}
