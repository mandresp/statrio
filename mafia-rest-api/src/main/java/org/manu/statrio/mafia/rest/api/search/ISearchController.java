package org.manu.statrio.mafia.rest.api.search;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/search")
public interface ISearchController {

	@GetMapping("/findAll")
	ResponseEntity<List<Gangster4ViewDto>> findAll();

	@GetMapping("/findBigBosses")
	ResponseEntity<List<GangsterBosses4ViewDto>> findBigBosses();

	@GetMapping("/findOne/{id}")
	ResponseEntity<Gangster4ViewDto> findOne(@PathVariable Integer id);

	@GetMapping("/findHierarchy/{id}")
	ResponseEntity<Gangster4ViewDto> findHierarchy(@PathVariable Integer id);

	@GetMapping("/findFullHierarchy")
	ResponseEntity<GangsterSubordinatesHierarchy4ViewDto> findFullHierarchy();

}
