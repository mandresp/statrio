package org.manu.statrio.mafia.rest.api.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(includeFieldNames = true)
public class GangsterBosses4ViewDto extends Gangster4ViewDto {

	private Integer subordinatesNumber;

}
