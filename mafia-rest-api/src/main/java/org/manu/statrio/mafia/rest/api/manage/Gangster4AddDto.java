package org.manu.statrio.mafia.rest.api.manage;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(includeFieldNames = true)
@ApiModel(description = "Data of the new Gangster to add to MAFIA")
public class Gangster4AddDto {

	@ApiModelProperty(value = "Name of the new Gangster")
	private String firstName;

	@ApiModelProperty(value = "Last name of the new Gangster")
	private String lastName;

	@ApiModelProperty(value = "ID of the boss of the new gangster")
	private Integer boss;

}
