package org.manu.statrio.mafia.rest.api.search;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(includeFieldNames = true)
public class Gangster4ViewDto {

	private Integer id;
	private String firstName;
	private String lastName;
	private Short rank;
	private String rankName;
	private boolean imprisoned;
	private Date joinDate;

}
