package org.manu.statrio.mafia.core.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.manu.statrio.mafia.core.api.dto.GangsterBossesDto;
import org.manu.statrio.mafia.core.api.dto.GangsterSubordinatesDto;
import org.manu.statrio.mafia.core.api.service.MafiaService;
import org.manu.statrio.mafia.model.api.dto.GangsterDto;
import org.manu.statrio.mafia.model.api.dto.GangsterRankEnum;
import org.manu.statrio.mafia.model.api.dto.GangsterRelationDto;
import org.manu.statrio.mafia.model.api.service.GangsterRelationService;
import org.manu.statrio.mafia.model.api.service.GangsterService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class MafiaServiceImpl implements MafiaService {

	@Autowired
	private GangsterService gangsterService;
	@Autowired
	private GangsterRelationService gangsterRelationService;

	@Override
	public List<GangsterBossesDto> findBigBosses() {

		List<GangsterBossesDto> bosses = new ArrayList<>();
		GangsterSubordinatesDto padrino = findFullHierarchy();
		calculateSize(padrino, bosses);
		return bosses;
	}

	@Override
	public GangsterSubordinatesDto findFullHierarchy() {
		GangsterDto padrino = gangsterService.findByRank(GangsterRankEnum.PADRINO.getId()).get(0);

		if (padrino.isImprisoned()) {
			return findHierarchy(padrino.getIdReplacement());
		}
		return findHierarchy(padrino.getId());
	}

	@Override
	public GangsterSubordinatesDto findHierarchy(Integer id) {
		final GangsterDto boss = gangsterService.findOne(id);
		if (boss.isImprisoned()) {
			return findHierarchy(boss.getIdReplacement());
		}
		return processHierarchy(boss);
	}

	/**
	 * @param id
	 * @param boss
	 * @return
	 */
	private GangsterSubordinatesDto processHierarchy(final GangsterDto boss) {

		GangsterSubordinatesDto bossHierarchy = new GangsterSubordinatesDto();
		BeanUtils.copyProperties(boss, bossHierarchy);

		List<GangsterRelationDto> subordinates = gangsterRelationService.findByBoss(boss.getId());
		processSubordinates(bossHierarchy, subordinates);
		findSustitutionHierarchy(boss, bossHierarchy);
		return bossHierarchy;

	}

	/**
	 * @param boss
	 * @param bossHierarchy
	 */
	private void findSustitutionHierarchy(final GangsterDto boss, GangsterSubordinatesDto bossHierarchy) {
		List<GangsterDto> imprisonedBosses = gangsterService.findByReplacementId(boss.getId());
		for (GangsterDto imprissioned : imprisonedBosses) {
			List<GangsterRelationDto> replacementSubordinates = gangsterRelationService.findByBoss(imprissioned.getId())
					.stream().filter(g -> !g.getIdGangsterSubordinate().equals(bossHierarchy.getId()))
					.collect(Collectors.toList());
			processSubordinates(bossHierarchy, replacementSubordinates);
			findSustitutionHierarchy(imprissioned, bossHierarchy);
		}
	}

	/**
	 * @param bossHierarchy
	 * @param subordinates
	 */
	private void processSubordinates(GangsterSubordinatesDto bossHierarchy, List<GangsterRelationDto> subordinates) {
		boolean anySubordinateFree = false;
		for (GangsterRelationDto subordinate : subordinates) {
			GangsterDto sub = gangsterService.findOne(subordinate.getIdGangsterSubordinate());
			if (!sub.isImprisoned()) {
				anySubordinateFree = true;
				bossHierarchy.getSubordinates().add(processHierarchy(sub));
			}
		}
		if (!anySubordinateFree) {
			GangsterDto sustitute = findSustitute(gangsterService.findOne(bossHierarchy.getId()), subordinates);
			if (sustitute != null && sustitute.getRank() > GangsterRankEnum.SOLDIER.getId()) {
				bossHierarchy.getSubordinates().add(processHierarchy(sustitute));
			}
		}

	}

	/**
	 * @param boss
	 * @param bosses
	 * @return size hierarchy
	 */
	private Integer calculateSize(GangsterSubordinatesDto boss, List<GangsterBossesDto> bosses) {
		Integer size = boss.getSubordinates().size();
		for (GangsterSubordinatesDto subordinate : boss.getSubordinates()) {
			size = size + calculateSize(subordinate, bosses);
		}
		if (size > 49) {
			GangsterBossesDto bigBoss = new GangsterBossesDto();
			BeanUtils.copyProperties(boss, bigBoss);
			bigBoss.setSubordinatesNumber(size);
			bosses.add(bigBoss);
		}
		return size;
	}

	// Transaccional
	@Override
	public void addGangster(String firstName, String lastName, Integer bossId) {

		GangsterDto boss = gangsterService.findOne(bossId);
		if (boss != null && boss.getRank() > GangsterRankEnum.SOLDIER.getId()) {
			GangsterDto dto = new GangsterDto();
			dto.setFirstName(firstName);
			dto.setImprisoned(false);
			dto.setLastName(lastName);
			dto.setJoinDate(new Date());
			dto.setRank(((short) (boss.getRank().intValue() - 1)));
			GangsterDto newG = gangsterService.saveGangster(dto);
			GangsterRelationDto rel = new GangsterRelationDto();
			rel.setCreationTime(new Date());
			rel.setIdGangsterBoss(bossId);
			rel.setIdGangsterSubordinate(newG.getId());
			gangsterRelationService.saveRelation(rel);
		}
	}

	@Override
	public void releaseGangster(Integer id) {
		GangsterDto gangster = gangsterService.findOne(id);
		gangster.setImprisoned(false);
		gangster.setIdReplacement(null);
		gangsterService.saveGangster(gangster);
	}

	@Override
	public void jailGangster(Integer id) {

		GangsterDto gangster = gangsterService.findOne(id);
		List<GangsterRelationDto> subs = gangsterRelationService.findByBoss(gangster.getId());
		gangster.setIdReplacement(findSustitute(gangster, subs).getId());
		gangster.setImprisoned(true);
		gangsterService.saveGangster(gangster);

	}

	// Transaccional
	@Override
	public void killGangster(Integer id) {

		GangsterDto gangster = gangsterService.findOne(id);
		List<GangsterRelationDto> subs = gangsterRelationService.findByBoss(gangster.getId());
		if (!subs.isEmpty()) {
			GangsterDto sustitute = findSustitute(gangster, subs);
			subs.stream().filter(s -> !s.getIdGangsterSubordinate().equals(sustitute.getId()))
					.collect(Collectors.toList()).stream().forEach(r -> r.setIdGangsterBoss(sustitute.getId()));

			GangsterRelationDto bossRel = gangsterRelationService.findBySubordinateId(gangster.getId());
			if (!sustitute.getRank().equals(gangster.getRank())) {
				sustitute.setRank(gangster.getRank());
				if (bossRel != null) {
					bossRel.setIdGangsterSubordinate(sustitute.getId());
					gangsterRelationService.saveRelation(bossRel);
				}
				gangsterService.saveGangster(sustitute);
			} else {
				gangsterRelationService.deleteRelation(bossRel);
			}
			gangsterRelationService.saveRelations(subs);
		}
		gangsterService.deleteGangster(gangster);

	}

	/**
	 * @param gangster
	 * @param subs2
	 * @param subs
	 * @return sustitute
	 */
	private GangsterDto findSustitute(GangsterDto gangster, List<GangsterRelationDto> subs) {
		List<GangsterDto> bossesSameRank = gangsterService.findByRank(gangster.getRank()).stream()
				.filter(g -> !g.isImprisoned() && !(g.getId().equals(gangster.getId()))).collect(Collectors.toList());
		if (!CollectionUtils.isEmpty(bossesSameRank)) {
			return bossesSameRank.stream().min(Comparator.comparing(GangsterDto::getJoinDate)).orElse(null);
		} else {
			List<GangsterDto> subordinates = gangsterService.findMany(
					subs.stream().map(GangsterRelationDto::getIdGangsterSubordinate).collect(Collectors.toList()));
			List<GangsterDto> freeSubordinates = subordinates.stream().filter(g -> !(g.isImprisoned()))
					.collect(Collectors.toList());

			if (!CollectionUtils.isEmpty(freeSubordinates)) {
				return freeSubordinates.stream().min(Comparator.comparing(GangsterDto::getJoinDate)).orElse(null);
			} else if (!CollectionUtils.isEmpty(subordinates)) {

				List<Integer> idsSubordinates = subs.stream().map(GangsterRelationDto::getIdGangsterSubordinate)
						.collect(Collectors.toList());

				GangsterDto lastFree = subordinates.stream()
						.filter(g -> g.getIdReplacement() != null && !idsSubordinates.contains(g.getIdReplacement()))
						.collect(Collectors.toList()).stream().min(Comparator.comparing(GangsterDto::getJoinDate))
						.orElse(null);

				return findSustitute(gangster, gangsterRelationService.findByBoss(lastFree.getId()));
			}
		}
		return null;
	}

}