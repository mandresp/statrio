package org.manu.statrio.mafia.core.api.service;

import java.util.List;

import org.manu.statrio.mafia.core.api.dto.GangsterBossesDto;
import org.manu.statrio.mafia.core.api.dto.GangsterSubordinatesDto;

/**
 * @author mapazos
 *
 */
/**
 * @author mapazos
 *
 */
public interface MafiaService {

	/**
	 * @return Mafia Hierarchy
	 */
	GangsterSubordinatesDto findFullHierarchy();

	/**
	 * @return Mafia Hierarchy
	 */
	GangsterSubordinatesDto findHierarchy(Integer id);

	/**
	 * @return mafia bosses
	 */
	List<GangsterBossesDto> findBigBosses();

	/**
	 * @param firstName
	 * @param lastName
	 * @param boss
	 */
	void addGangster(String firstName, String lastName, Integer boss);

	/**
	 * @param id
	 */
	void jailGangster(Integer id);

	/**
	 * @param id
	 */
	void killGangster(Integer id);

	/**
	 * @param id
	 */
	void releaseGangster(Integer id);

}
