package org.manu.statrio.mafia.core.api.dto;

import org.manu.statrio.mafia.model.api.dto.GangsterDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString(includeFieldNames = true)
public class GangsterBossesDto extends GangsterDto {

	private Integer subordinatesNumber;

}
